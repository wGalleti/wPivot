'use strict';
(function() {
    window.wPivot = {
        getDistinct: function(json, header) {
            // return object
            var result = [];
            // check integrit
            if (typeof(json) == 'undefined' || typeof(header) == 'undefined') {
                return null;
            }
            // aux object
            var keys = [];
            // loop in data for check values and add with key in aux object
            for (var key in json) {
                // check header is present in data
                if (json[key].hasOwnProperty(header)) {
                    // add value in key
                    keys[json[key][header]] = [];
                }
            }
            // convert aux objects in array
            for (var element in keys) {
                result.push(element);
            }
            // return new array
            return result;
        },
        joinElements: function(element1, element2) {
            // check integrit
            if (typeof(element1) == 'undefined' && typeof(element2) == 'undefined') {
                return null;
            }
            var result = [];
            for (var key in element1) {
                result[element1[key]] = [];
                for (var i = 0; i < element2.length; i++) {
                    result[element1[key]].push(element2[i]);
                }
            }
            return result;
        },
        fillElements: function(base, data, colVal, ColX, ColY) {
            console.log(base);
            // check integrit
            if (typeof(base) == 'undefined' && typeof(data) == 'undefined') {
                return null;
            }
            var result = [];
            for (var key in base) {
                result[key] = [];
                if (typeof(base[key]) == 'object') {
                    for (var child in base[key]) {
                        result[key][base[key][child]] = wPivot.getValue(data, colVal, ColX, ColY, base[key][child], key)
                    }
                };
            }
            return result;
        },
        getValue: function(data, colVal, ColX, ColY, Valx, Valy) {
            var sum = 0;
            for (var key in data) {
                if (data[key][ColX] == Valx && data[key][ColY] == Valy) {
                    sum += data[key][colVal];
                }
            }
            return sum;
        }
    }
})();